angular.module 'piperka.services', []

# Note to self: Due to cross-domain shenanigans, you can't use a cookie to store a session.
# Instead, I will have to send a post request with username and password set every time.
# Should put a warning in the app that Piperka does not support https and that
# the app, nor the site itself, should be used if their piperka credentials are valuable.

# Currently working something out with kaol regarding this.

.factory 'UpdateService', ['$http', '$q', 'LoginService', ($http, $q, LoginService)->
  all: (login)->
    [username, token] = LoginService.get()
    $http.get("http://piperka.net/s/uprefs?token=#{token}").then (response)->
      if response.data.subscriptions
        updates = for comic in response.data.subscriptions
          [id, currentPage, totalPages, offset, remainingPages] = comic
          {id: id, count: remainingPages}
        updates
      else
        null

  cache: (updates)->
    localStorage.setItem('updates', angular.toJson(updates))

  cached: ->
    updates = localStorage.getItem('updates')
    updates && angular.fromJson(updates)

  location: (comic)->
    [username, token] = LoginService.get()
    "http://piperka.net/updates.html?redir=#{comic.id}&csrf_ham=#{token}"

  titles: ->
    $http.get('http://piperka.net/d/comictitles').then (response)->
      localStorage.setItem('titles', angular.fromJson(response.data))
      response.data

  cachedTitles: ->
    titles = localStorage.getItem('titles')
    titles && angular.toJson(titles)

  mergeTitles: (updates, titles)->
    return null unless updates and titles
    for update in updates
      if titles[update.id]
        update.title = titles[update.id]
      else
        return null
    updates
]

.factory 'LoginService', ['$http', ($http)->
  get: ->
    [localStorage.getItem('username'), localStorage.getItem('token')]
  login: (login)->
    $http.post "http://piperka.net/s/login?user=#{login.username}&passwd=#{login.password}"
    .success (data)->
      token = data.csrf_ham
      name = data.name
      localStorage.setItem('token', token)
      localStorage.setItem('username', name)
      data
]
