angular.module "piperka.controllers", []

.controller "UpdatesCtrl", ['$scope', '$rootScope', '$timeout', '$location', '$ionicScrollDelegate', 'UpdateService', ($scope, $rootScope, $timeout, $location, $ionicScrollDelegate, UpdateService)->
  $scope.search = {}

  $scope.$watch 'search.title', ->
    $ionicScrollDelegate.scrollTop(true)

  $scope.abort = ->
    $rootScope.isLoading = false
    null

  $scope.refreshUpdates = ->
    $rootScope.isLoading = true
    $scope.updates = UpdateService.cached()
    UpdateService.all().then (updates)->
      unless updates
        $scope.failed = true
        return $scope.abort()
      if updates.length == 0
        $scope.noUpdates = true
        return $scope.abort()
      titles = UpdateService.cachedTitles()
      titledUpdates = UpdateService.mergeTitles(updates, titles)
      if titledUpdates
        $scope.setUpdates(titledUpdates)
        return $scope.abort()
      else
        $rootScope.isLoading = true
        UpdateService.titles().then (titles)->
          titledUpdates = UpdateService.mergeTitles(updates, titles)
          $scope.setUpdates(titledUpdates)
          return $scope.abort()

  $scope.setUpdates = (updates)->
    $scope.updates = updates
    UpdateService.cache(updates)

  $scope.refreshUpdates()

  $scope.redirectTo = (update)->
    $scope.url = UpdateService.location(update)
    $scope.updates = (u for u in $scope.updates when u.id != update.id)
    $timeout -> # We use $timeout because Angular gets bitchy about using window otherwise.
      if navigator.platform == 'android'
        navigator.app.loadUrl($scope.url, {openExternal: true, showLocationBar: false})
      else
        window.open($scope.url, '_system', 'location=no')
      false

  $scope.countFilter = (update)->
    update.count > 0
]

.controller "LoginCtrl", ['$scope', '$rootScope', 'LoginService', ($scope, $rootScope, LoginService)->
  [$scope.name, $scope.token] = LoginService.get()
  $scope.credentials = {}
  $rootScope.isLoading = false
  $scope.login = ->
    $rootScope.isLoading = true
    LoginService.login($scope.credentials)
    .success (data)->
      $rootScope.isLoading = false
      if data.csrf_ham
        [$scope.name, $scope.token] = [data.name, data.csrf_ham]
]