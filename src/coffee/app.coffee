# Ionic piperka App

# angular.module is a global place for creating,
# registering and retrieving Angular modules
# 'piperka' is the name of this angular module example
# (also set in a <body> attribute in index.html)
# the 2nd parameter is an array of 'requires'
# 'piperka.services' is found in services.js
# 'piperka.controllers' is found in controllers.js
angular.module 'piperka', ['ionic', 'piperka.services', 'piperka.controllers']

.config ($stateProvider, $urlRouterProvider) ->
  # Ionic uses AngularUI Router which uses the concept of states
  # Learn more here: https://github.com/angular-ui/ui-router
  # Set up the various states which the app can be in.
  # Each state's controller can be found in controllers.js
  
  $stateProvider
  
    # setup an abstract state for the tabs directive
    .state 'tab',
      url: '/tab'
      abstract: true
      templateUrl: 'templates/tabs.html'

    # Piperka app states here
    .state 'tab.updates',
      url: '/updates'
      views:
        'updates-tab':
          templateUrl: 'templates/updates.html'
          controller: 'UpdatesCtrl'

    .state 'tab.login',
      url: '/login'
      views:
        'login-tab':
          templateUrl: 'templates/login.html'
          controller: 'LoginCtrl'
    
  # if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise '/tab/updates'
