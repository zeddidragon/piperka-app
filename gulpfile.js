var gulp = require('gulp');
var browserify = require('gulp-browserify');
var concat = require('gulp-concat');
var styl = require('gulp-styl');
var path = require('path');
var o = require('open');
var ripple = require('ripple-emulator');
var coffee = require('gulp-coffee')
var haml = require('gulp-ruby-haml')
var sass = require('gulp-sass')

// Builds the scripts based on a single entry point using browserify
gulp.task('scripts', function() {
  return gulp.src(['./src/coffee/**/*.coffee'])
    .pipe(coffee())
    .pipe(gulp.dest('./www/js'))
});
 
// Concatenates all the CSS files together.
gulp.task('styles', function() {
  return gulp.src(['./src/sass/**/*.sass'])
    .pipe(sass())
    .pipe(gulp.dest('./www/css'))
});

gulp.task('markup', function(){
  return gulp.src(['./src/haml/**/*.haml'])
    .pipe(haml())
    .pipe(gulp.dest('./www'))
});
 
// The default task
gulp.task('default', ['scripts', 'styles', 'markup'], function() {
  gulp.watch('./src/coffee/**', ['scripts']);
  gulp.watch('./src/sass/**', ['styles']);
  gulp.watch('./src/haml/**', ['markup']);
 
  var options = {
    keepAlive: false,
    open: true,
    port: 4400
  };
 
  // Start the ripple server
  ripple.emulate.start(options);
 
  if (options.open) {
    o('http://localhost:' + options.port + '?enableripple=cordova-3.0.0');
  }
});
